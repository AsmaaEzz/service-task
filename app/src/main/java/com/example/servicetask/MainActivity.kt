package com.example.servicetask

import android.annotation.SuppressLint
import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()


        val intentFilter = IntentFilter()
        intentFilter.addAction(NumService().MY_ACTION)
        registerReceiver(myReceiver, intentFilter)

        //Start our own service
        val intent = Intent(
            this, NumService::class.java
        )
        startService(intent)


    }

    private val myReceiver = object : BroadcastReceiver() {
        var datapassed = 0
        override fun onReceive(p0: Context?, p1: Intent?) {
            if (p1 != null) {
                datapassed = p1.getIntExtra("DATAPASSED", 0)
                Log.d(ContentValues.TAG, "received " + datapassed)
                val t :TextView = findViewById(R.id.textview)

                t.setText(datapassed.toString())
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(myReceiver)
    }
}



