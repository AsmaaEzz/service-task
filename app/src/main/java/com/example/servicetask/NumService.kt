package com.example.servicetask

import android.app.IntentService
import android.app.Service
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.IBinder
import android.util.Log

class NumService:Service(){
    var counter = -1
    val MY_ACTION = "MY_ACTION"
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG,"service started")
        Thread(Runnable {
            while(true){
                counter++
                Thread.sleep(3000)
                val intent = Intent()
                intent.action = MY_ACTION

                intent.putExtra("DATAPASSED", counter)

                sendBroadcast(intent)
                Log.d(TAG,"number passed"+ counter)

            }
        }).start()
        return START_NOT_STICKY

    }

    override fun onDestroy() {
        super.onDestroy()
        stopSelf()
    }


}